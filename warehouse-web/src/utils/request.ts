import axios, {AxiosInstance} from 'axios';

const devUrl = 'http://localhost:8078';
const proUrl = 'http://124.70.63.154:8078';

export const BASE_URL: string = process.env.NODE_ENV === 'development' ? devUrl : proUrl;

const instance = axios.create({
   baseURL: BASE_URL
});
instance.interceptors.request.use((config: any) => {
   return config;
}, (error: any) => {
   return error;
});
instance.interceptors.response.use((res: { data: any }) => {
   return res.data;
}, (error: any) => {
   return Promise.reject(error);
});

export default instance as any | AxiosInstance;
