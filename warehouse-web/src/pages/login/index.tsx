import React from 'react';
import {message, Typography} from 'antd';
import ProForm, {ProFormText} from '@ant-design/pro-form';
import {UserOutlined} from '@ant-design/icons';
import styles from './index.less';
import {useHistory} from 'react-router-dom';

export default function Login() {
   const history = useHistory();
   return (
      <div className={styles.layout}>
         <div
            style={{
               margin: 'auto'
            }}
         >
            <ProForm validateMessages={{required: '必填项'}}
                     onFinish={async (event) => {
                        if (event.username === 'admin' && event.password === 'admin2021') {
                           sessionStorage.setItem('tn', window.btoa(JSON.stringify(event)));
                           message.success({
                              content: '登录成功，正在跳转',
                              duration: 0.8
                           });
                           history.push('/home');
                        } else {
                           message.warn({
                              content: '用户名或密码不正确',
                              key: 1000
                           });
                        }

                     }}
                     submitter={{
                        searchConfig: {
                           submitText: '登录'
                        },
                        render: (_, dom) => dom.pop(),
                        submitButtonProps: {
                           size: 'large',
                           style: {
                              width: '100%'
                           }
                        }
                     }}
            >
               <h1 style={{textAlign: 'center', margin: '30px 0'}}>
                  仓库管理系统
               </h1>
               <Typography className={styles.note}>
                  <Typography.Text>
                     <span style={{color: '#a0a0a0', fontSize: '16px'}}>本系统可供管理员登录使用</span>
                  </Typography.Text>
               </Typography>
               <ProFormText
                  fieldProps={{
                     size: 'large',
                     prefix: <UserOutlined />
                  }}
                  name='username'
                  placeholder='请输入用户名'
                  rules={[
                     {
                        required: true
                     }
                  ]}
               />
               <ProFormText
                  fieldProps={{
                     size: 'large',
                     type: 'password',
                     prefix: <UserOutlined />
                  }}
                  name='password'
                  rules={[
                     {
                        required: true
                     }
                  ]}
                  placeholder='请输入密码'
               />
            </ProForm>
         </div>
      </div>
   );
};
