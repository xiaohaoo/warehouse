import {Typography} from 'antd';
import React from 'react';
import styles from './index.less';

export default () => {
   const dateTime = React.useState<string>(() => {
      return new Date().toLocaleString();
   });

   React.useEffect(() => {
      const interval = setInterval(() => {
         dateTime[1](new Date().toLocaleString());
      }, 1000);
      return () => {
         clearInterval(interval);
      };
   }, []);

   return <Typography className={styles.layout}>
      <Typography.Title level={1} style={{textAlign: 'center', fontSize: '30px'}}>
         仓库管理系统
      </Typography.Title>
      <Typography.Title level={5} style={{textAlign: 'center', fontSize: '15px'}}>
         {dateTime[0].toLocaleString()}
      </Typography.Title>
      <Typography.Paragraph>
      </Typography.Paragraph>
   </Typography>;

}
