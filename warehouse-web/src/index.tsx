import React from 'react';
import ReactDOM from 'react-dom';
import Layout from '@/layout';
import '@/common/index.less';
import {HashRouter, Route, Switch} from 'react-router-dom';
import Login from '@/pages/login';
import zhCN from 'antd/lib/locale/zh_CN';
import {ConfigProvider} from 'antd';

ReactDOM.render(
   <React.StrictMode>
      <ConfigProvider locale={zhCN}>
         <HashRouter>
            <Switch>
               <Route path='/login' exact component={Login} />
               <Layout />
            </Switch>
         </HashRouter>
      </ConfigProvider>
   </React.StrictMode>,
   document.getElementById('root')
);
