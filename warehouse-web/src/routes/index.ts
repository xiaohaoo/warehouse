import {RouteConfig} from 'react-router-config';
import {Route} from '@ant-design/pro-layout/lib/typings';
import {RightOutlined} from '@ant-design/icons';
import React from 'react';

import Warehouse from '@/pages/warehouse';
import Home from '@/pages/home';
import Vehicle from "@/pages/vehicle";


export default [
   {
      path: '/home',
      component: Home,
      name: '系统首页',
      icon: React.createElement(RightOutlined)
   },

   {
      name: '仓库信息',
      path: '/warehouse',
      component: Warehouse,
      icon: React.createElement(RightOutlined)
   },
   {
      name: '车辆信息',
      path: '/vehicle',
      component: Vehicle,
      icon: React.createElement(RightOutlined)
   },

] as RouteConfig[] & Route[];
