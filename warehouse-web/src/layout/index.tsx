import React, {useState} from 'react';
import {Avatar, Dropdown, Menu, message} from 'antd';
import {UserOutlined} from '@ant-design/icons';
import routes from '@/routes';
import ProLayout from '@ant-design/pro-layout';
import {renderRoutes} from 'react-router-config';
import {useHistory} from 'react-router-dom';

export default () => {
   const pathnameState = useState<string | undefined>('/welcome');
   const history = useHistory();
   React.useEffect(() => {
      const token = sessionStorage.getItem('tn');
      if (!token) {
         message.warn('尚未登录，请先登录');
         history.replace('/login');
      }
   }, []);
   return <div
      id='test-pro-layout'
      style={{height: '100vh'}}
   >
      <ProLayout
         route={{routes}}
         fixSiderbar={true}
         headerTheme='dark'
         fixedHeader={true}
         title='仓库管理系统'
         navTheme='dark'
         location={{pathname: pathnameState[0]}}
         menuItemRender={(item, dom) => (
            <a onClick={() => {
               pathnameState[1](item.path);
               item.path && history.push(item.path);
            }}
            >{dom}</a>
         )}

         rightContentRender={() => (
            <Dropdown overlay={<Menu>
               <Menu.Item danger onClick={() => {
                  history.replace('/login');
               }}>退出</Menu.Item>
            </Menu>}>
               <Avatar style={{height:'30px'}} shape='square' size='small' icon={<UserOutlined style={{lineHeight:'30px'}}/>} />
            </Dropdown>
         )}
      >
         {
            renderRoutes(routes)
         }
      </ProLayout>
   </div>;
};

