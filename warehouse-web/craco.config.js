const CracoLessPlugin = require('craco-less');
const path = require('path');
const WebpackBar = require('webpackbar');

module.exports = {
   eslint: {
      enable: false
   },
   webpack: {
      plugins: [new WebpackBar()],
      alias: {
         '@@': path.resolve('.'),
         '@': path.resolve('.', 'src')
      },
      configure: (webpackConfig, {env}) => {
          webpackConfig.output.publicPath = ''
          return webpackConfig
      },
   },
   plugins: [
      {
         plugin: CracoLessPlugin,
         options: {
            cssLoaderOptions: {
               modules: {
                  exportLocalsConvention: 'camelCaseOnly',
                  auto: (resourcePath) => {
                     return !resourcePath.includes('node_modules') && resourcePath.endsWith('.less');
                  }
               },
               sourceMap: true
            },
            lessLoaderOptions: {
               lessOptions: {
                  javascriptEnabled: true
               }
            }
         }
      }
   ]
};
