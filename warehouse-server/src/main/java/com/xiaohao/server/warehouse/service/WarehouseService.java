package com.xiaohao.server.warehouse.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaohao.server.warehouse.entity.Warehouse;
import com.xiaohao.server.warehouse.mapper.WarehouseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class WarehouseService {
    @Autowired
    private WarehouseMapper warehouseMapper;

    public Warehouse queryById(Integer id) {
        return warehouseMapper.selectById(id);
    }

    public Warehouse queryOne(Warehouse warehouse) {
        return warehouseMapper.selectOne(new QueryWrapper<>(warehouse));
    }

    public List<Warehouse> queryList(Warehouse warehouse) {
        LambdaQueryWrapper<Warehouse> queryWrapper =
            new LambdaQueryWrapper<>(warehouse);
        return warehouseMapper.selectList(queryWrapper);
    }

    public Integer deleteById(Integer id) {
        return warehouseMapper.deleteById(id);
    }

    public Integer save(Warehouse warehouse) {
        if (Objects.nonNull(warehouse) && Objects.nonNull(warehouse.getId())) {
            return warehouseMapper.updateById(warehouse);
        } else {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            warehouse.setTime(timestamp);
            return warehouseMapper.insert(warehouse);
        }
    }
}
