package com.xiaohao.server.warehouse.controller;

import com.xiaohao.server.warehouse.entity.ResponseResult;
import com.xiaohao.server.warehouse.entity.Warehouse;
import com.xiaohao.server.warehouse.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("warehouse")
public class WarehouseController {
    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("{id}")
    public ResponseResult<Warehouse> queryById(@PathVariable Integer id) {
        return ResponseResult.success(warehouseService.queryById(id));
    }


    @GetMapping
    public ResponseResult<List<Warehouse>> queryList(Warehouse warehouse) {
        return ResponseResult.success(warehouseService.queryList(warehouse));
    }

    @DeleteMapping("{id}")
    public ResponseResult<Void> delete(@PathVariable Integer id) {
        return warehouseService.deleteById(id) > 0 ? ResponseResult.success() : ResponseResult.error();
    }

    @PostMapping
    public ResponseResult<Void> save(@RequestBody Warehouse warehouse) {
        return warehouseService.save(warehouse) > 0 ? ResponseResult.success() : ResponseResult.error();
    }
}
