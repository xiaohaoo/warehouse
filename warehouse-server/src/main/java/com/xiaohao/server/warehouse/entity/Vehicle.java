package com.xiaohao.server.warehouse.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TableName("vehicle")
@Table(name = "vehicle")
@Accessors(chain = true)
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 车牌号
     */
    private String code;

    /**
     * 车型
     */
    private String type;

    private  Double oil;

    private Integer standardOilUnit;

    private Integer actualOilUnit;

    private Double mileage;

    private Double assessment;

    private String driver;

    private String note;

    /**
     * 节省金额
     */
    private Double savings;


    /**
     * 兑现金额
     */
    private Double cash;

    private Timestamp time;

}
