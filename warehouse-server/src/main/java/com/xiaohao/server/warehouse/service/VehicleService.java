package com.xiaohao.server.warehouse.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaohao.server.warehouse.entity.Vehicle;
import com.xiaohao.server.warehouse.mapper.VehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Service
public class VehicleService {
    @Autowired
    private VehicleMapper vehicleMapper;

    public Vehicle queryById(Integer id) {
        return vehicleMapper.selectById(id);
    }

    public Vehicle queryOne(Vehicle vehicle) {
        return vehicleMapper.selectOne(new QueryWrapper<>(vehicle));
    }

    public List<Vehicle> queryList(Vehicle warehouse) {
        LambdaQueryWrapper<Vehicle> queryWrapper =
            new LambdaQueryWrapper<>(warehouse);
        return vehicleMapper.selectList(queryWrapper);
    }

    public Integer deleteById(Integer id) {
        return vehicleMapper.deleteById(id);
    }

    public Integer save(Vehicle vehicle) {
        if (Objects.nonNull(vehicle) && Objects.nonNull(vehicle.getId())) {
            return vehicleMapper.updateById(vehicle);
        } else {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            vehicle.setTime(timestamp);
            return vehicleMapper.insert(vehicle);
        }
    }
}
