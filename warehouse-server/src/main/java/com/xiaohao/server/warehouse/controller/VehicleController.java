package com.xiaohao.server.warehouse.controller;

import com.xiaohao.server.warehouse.entity.ResponseResult;
import com.xiaohao.server.warehouse.entity.Vehicle;
import com.xiaohao.server.warehouse.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vehicle")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    @GetMapping("{id}")
    public ResponseResult<Vehicle> queryById(@PathVariable Integer id) {
        return ResponseResult.success(vehicleService.queryById(id));
    }


    @GetMapping
    public ResponseResult<List<Vehicle>> queryList(Vehicle vehicle) {
        return ResponseResult.success(vehicleService.queryList(vehicle));
    }

    @DeleteMapping("{id}")
    public ResponseResult<Void> delete(@PathVariable Integer id) {
        return vehicleService.deleteById(id) > 0 ? ResponseResult.success() : ResponseResult.error();
    }

    @PostMapping
    public ResponseResult<Void> save(@RequestBody Vehicle vehicle) {
        return vehicleService.save(vehicle) > 0 ? ResponseResult.success() : ResponseResult.error();
    }
}
