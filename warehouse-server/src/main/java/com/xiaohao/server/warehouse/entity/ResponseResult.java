package com.xiaohao.server.warehouse.entity;

import lombok.Getter;

@Getter
public class ResponseResult<T> {
    private final Integer code;
    private final T data;
    private final String message;

    private ResponseResult(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    private enum ResultCode {
        SUCCESS(200, "成功"), ERROR(500, "服务器错误，请联系管理员");
        private final int code;
        private final String message;

        ResultCode(int code, String message) {
            this.code = code;
            this.message = message;
        }

    }


    public static <T> ResponseResult<T> success(T data) {
        return new ResponseResult<>(ResultCode.SUCCESS.code, ResultCode.SUCCESS.message, data);
    }

    public static ResponseResult<Void> success(String message) {
        return new ResponseResult<>(ResultCode.SUCCESS.code, message, null);
    }

    public static ResponseResult<Void> success() {
        return new ResponseResult<>(ResultCode.SUCCESS.code, ResultCode.SUCCESS.message, null);
    }

    public static <T> ResponseResult<T> success(String message, T data) {
        return new ResponseResult<>(ResultCode.SUCCESS.code, message, data);
    }

    public static ResponseResult<Void> error(String message) {
        return new ResponseResult<>(ResultCode.ERROR.code, message, null);
    }

    public static <T> ResponseResult<T> error(T data) {
        return new ResponseResult<>(ResultCode.ERROR.code, ResultCode.ERROR.message, data);
    }

    public static <T> ResponseResult<T> error(String message, T data) {
        return new ResponseResult<>(ResultCode.ERROR.code, message, data);
    }

    public static <Void> ResponseResult<Void> error() {
        return new ResponseResult<>(ResultCode.ERROR.code, ResultCode.ERROR.message, null);
    }

}


