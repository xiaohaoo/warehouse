package com.xiaohao.server.warehouse.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@TableName("warehouse")
@Table(name = "warehouse")
@Accessors(chain = true)
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private Double unitPrice;
    private Integer applyNumber;
    private Integer actualNumber;
    private Timestamp time;
    private String note;
    private Double totalPrice;
    private String unit;
}
